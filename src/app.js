/** @jsx React.DOM */

var Alert = React.createClass({
  render: function() {
    return(
      <div data-alert className="alert-box">
        Alert goes here...
        <a href="#" className="close">&times;</a>
      </div>
    );
  }
});

var Comments = React.createClass({
  getInitialState: function() {
    return { previousComments: [] };
  },
  componentWillMount: function() {
    $.ajax({
      url: '/comments',
      dataType: 'json',
      type: 'GET',
      success: function(data) {
        this.setState({previousComments: data});
      }.bind(this)
    });
  },
  render: function() {
    return(
      <div className="large-6 columns">
        <h2>Comments</h2>
        <Comment
          email={this.props.email}
          message={this.props.message}
          name={this.props.name}
        />
        {
          this.state.previousComments.map(function(comment) {
            return(
              <Comment
                email={comment.email}
                message={comment.message}
                name={comment.name}
              />
            );
          })
        }
      </div>
    );
  }
});

var Message = React.createClass({
  converter: new Showdown.converter(),
  render: function() {
    var markup = this.converter.makeHtml(this.props.message);
    return(
      <div
        className="large-9 columns"
        dangerouslySetInnerHTML={{__html: markup}}
      >
      </div>
    );
  }
});

var AuthorAvatar = React.createClass({
  render: function() {
    var imageUrl = "http://www.gravatar.com/avatar/" + CryptoJS.MD5(this.props.email);
    return(
      <div className="large-3 columns">
        <a className="th">
          <img src={imageUrl}></img>
        </a>
      </div>
    );
  }
});

var AuthorSignature = React.createClass({
  render: function() {
    return(
      <div className="signature">{this.props.name}</div>
    );
  }
});

var Comment = React.createClass({
  render: function() {
    return(
      <article className="panel radius row">
        <Message message={this.props.message} />
        <AuthorAvatar email={this.props.email} />
        <AuthorSignature name={this.props.name} />
      </article>
    );
  }
});

var SubmitButton = React.createClass({
  handleClick: function() {
    this.props.onSubmitComment();
  },
  render: function() {
    return(
      <div className="row">
        <div className="large-12 columns">
          <a className="button radius large" onClick={this.handleClick} >
            Leave your comment
          </a>
        </div>
      </div>
    );
  }
});

var CommentForm = React.createClass({
  saveComment: function() {
    var email   = this.refs.email.getDOMNode().value,
        message = this.refs.message.getDOMNode().value,
        name    = this.refs.name.getDOMNode().value;
    $.ajax({
      url: '/comments',
      dataType: 'json',
      type: 'POST',
      data: { email: email, name: name, message: message }
    }).success(this.props.onSubmitButtonClick);
  },
  handleMessageChange: function() {
    var message = this.refs.message.getDOMNode().value;
    this.props.onMessageChange(message);
  },
  handleEmailChange: function() {
    var email = this.refs.email.getDOMNode().value;
    this.props.onEmailChange(email);
  },
  handleNameChange: function() {
    var name = this.refs.name.getDOMNode().value;
    this.props.onNameChange(name)
  },
  handleSubmitButtonClick: function() {
    this.saveComment();
  },
  render: function() {
    return(
      <div className="large-5 columns">
        <div className="row">
          <h2>Write Something</h2>
          <form>
            <div className="row">
              <div className="large-12 columns">
                <textarea
                  ref="message"
                  placeholder="Write your message in markdown..."
                  onChange={this.handleMessageChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="large-6 columns">
                <label>name</label>
                <input
                  ref="name"
                  type="text"
                  placeholder="John Doe"
                  onChange={this.handleNameChange}
                />
              </div>
              <div className="large-6 columns">
                <label>email address</label>
                <input
                  ref="email"
                  type="text"
                  placeholder="john.doe@example.com"
                  onChange={this.handleEmailChange}
                />
              </div>
            </div>
            <SubmitButton
              onSubmitComment={this.handleSubmitButtonClick}
            />
          </form>
        </div>
      </div>
    );
  }
});

var App = React.createClass({
  getInitialState: function() {
    return {email: '', message: '', name: ''};
  },
  handleMessageChange: function(message) {
    this.setState({
      email: this.state.email,
      message: message,
      name: this.state.email
    });
  },
  handleEmailChange: function(email) {
    this.setState({
      email: email,
      message: this.state.message,
      name: this.state.name
    });
  },
  handleNameChange: function(name) {
    this.setState({
      email: this.state.email,
      message: this.state.message,
      name: name
    });
  },
  handleSubmitButtonClick: function() {
    this.setState({email: '', message: '', name: ''});
  },
  render: function() {
    return(
      <div>
        <Alert />
        <div id="content" className="row">
          <Comments
            ref="comments"
            email={this.state.email}
            message={this.state.message}
            name={this.state.name}
          />
          <CommentForm
            email={this.state.email}
            message={this.state.message}
            name={this.state.name}
            onMessageChange={this.handleMessageChange}
            onEmailChange={this.handleEmailChange}
            onNameChange={this.handleNameChange}
            onSubmitButtonClick={this.handleSubmitButtonClick}
          />
        </div>
      </div>
    );
  }
});


React.renderComponent(
  <App />,
  document.getElementById("app")
);
