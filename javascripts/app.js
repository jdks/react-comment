/** @jsx React.DOM */

var Alert = React.createClass({displayName: 'Alert',
  render: function() {
    return(
      React.DOM.div( {'data-alert':true, className:"alert-box"}, 
        " Alert goes here... ",
        React.DOM.a( {href:"#", className:"close"}, "×")
      )
    );
  }
});

var Comments = React.createClass({displayName: 'Comments',
  getInitialState: function() {
    return { previousComments: [] };
  },
  componentWillMount: function() {
    $.ajax({
      url: '/comments',
      dataType: 'json',
      type: 'GET',
      success: function(data) {
        this.setState({previousComments: data});
      }.bind(this)
    });
  },
  render: function() {
    return(
      React.DOM.div( {className:"large-6 columns"}, 
        React.DOM.h2(null, "Comments"),
        Comment(
          {email:this.props.email,
          message:this.props.message,
          name:this.props.name}
        ),
        
          this.state.previousComments.map(function(comment) {
            return(
              Comment(
                {email:comment.email,
                message:comment.message,
                name:comment.name}
              )
            );
          })
        
      )
    );
  }
});

var Message = React.createClass({displayName: 'Message',
  converter: new Showdown.converter(),
  render: function() {
    var markup = this.converter.makeHtml(this.props.message);
    return(
      React.DOM.div(
        {className:"large-9 columns",
        dangerouslySetInnerHTML:{__html: markup}}
      
      )
    );
  }
});

var AuthorAvatar = React.createClass({displayName: 'AuthorAvatar',
  render: function() {
    var imageUrl = "http://www.gravatar.com/avatar/" + CryptoJS.MD5(this.props.email);
    return(
      React.DOM.div( {className:"large-3 columns"}, 
        React.DOM.a( {className:"th"}, 
          React.DOM.img( {src:imageUrl})
        )
      )
    );
  }
});

var AuthorSignature = React.createClass({displayName: 'AuthorSignature',
  render: function() {
    return(
      React.DOM.div( {className:"signature"}, this.props.name)
    );
  }
});

var Comment = React.createClass({displayName: 'Comment',
  render: function() {
    return(
      React.DOM.article( {className:"panel radius row"}, 
        Message( {message:this.props.message} ),
        AuthorAvatar( {email:this.props.email} ),
        AuthorSignature( {name:this.props.name} )
      )
    );
  }
});

var SubmitButton = React.createClass({displayName: 'SubmitButton',
  handleClick: function() {
    this.props.onSubmitComment();
  },
  render: function() {
    return(
      React.DOM.div( {className:"row"}, 
        React.DOM.div( {className:"large-12 columns"}, 
          React.DOM.a( {className:"button radius large", onClick:this.handleClick} , 
            " Leave your comment "
          )
        )
      )
    );
  }
});

var CommentForm = React.createClass({displayName: 'CommentForm',
  saveComment: function() {
    var email   = this.refs.email.getDOMNode().value,
        message = this.refs.message.getDOMNode().value,
        name    = this.refs.name.getDOMNode().value;
    $.ajax({
      url: '/comments',
      dataType: 'json',
      type: 'POST',
      data: { email: email, name: name, message: message }
    }).success(this.props.onSubmitButtonClick);
  },
  handleMessageChange: function() {
    var message = this.refs.message.getDOMNode().value;
    this.props.onMessageChange(message);
  },
  handleEmailChange: function() {
    var email = this.refs.email.getDOMNode().value;
    this.props.onEmailChange(email);
  },
  handleNameChange: function() {
    var name = this.refs.name.getDOMNode().value;
    this.props.onNameChange(name)
  },
  handleSubmitButtonClick: function() {
    this.saveComment();
  },
  render: function() {
    return(
      React.DOM.div( {className:"large-5 columns"}, 
        React.DOM.div( {className:"row"}, 
          React.DOM.h2(null, "Write Something"),
          React.DOM.form(null, 
            React.DOM.div( {className:"row"}, 
              React.DOM.div( {className:"large-12 columns"}, 
                React.DOM.textarea(
                  {ref:"message",
                  placeholder:"Write your message in markdown...",
                  onChange:this.handleMessageChange}
                )
              )
            ),
            React.DOM.div( {className:"row"}, 
              React.DOM.div( {className:"large-6 columns"}, 
                React.DOM.label(null, "name"),
                React.DOM.input(
                  {ref:"name",
                  type:"text",
                  placeholder:"John Doe",
                  onChange:this.handleNameChange}
                )
              ),
              React.DOM.div( {className:"large-6 columns"}, 
                React.DOM.label(null, "email address"),
                React.DOM.input(
                  {ref:"email",
                  type:"text",
                  placeholder:"john.doe@example.com",
                  onChange:this.handleEmailChange}
                )
              )
            ),
            SubmitButton(
              {onSubmitComment:this.handleSubmitButtonClick}
            )
          )
        )
      )
    );
  }
});

var App = React.createClass({displayName: 'App',
  getInitialState: function() {
    return {email: '', message: '', name: ''};
  },
  handleMessageChange: function(message) {
    this.setState({
      email: this.state.email,
      message: message,
      name: this.state.email
    });
  },
  handleEmailChange: function(email) {
    this.setState({
      email: email,
      message: this.state.message,
      name: this.state.name
    });
  },
  handleNameChange: function(name) {
    this.setState({
      email: this.state.email,
      message: this.state.message,
      name: name
    });
  },
  handleSubmitButtonClick: function() {
    this.setState({email: '', message: '', name: ''});
  },
  render: function() {
    return(
      React.DOM.div(null, 
        Alert(null ),
        React.DOM.div( {id:"content", className:"row"}, 
          Comments(
            {ref:"comments",
            email:this.state.email,
            message:this.state.message,
            name:this.state.name}
          ),
          CommentForm(
            {email:this.state.email,
            message:this.state.message,
            name:this.state.name,
            onMessageChange:this.handleMessageChange,
            onEmailChange:this.handleEmailChange,
            onNameChange:this.handleNameChange,
            onSubmitButtonClick:this.handleSubmitButtonClick}
          )
        )
      )
    );
  }
});


React.renderComponent(
  App(null ),
  document.getElementById("app")
);
